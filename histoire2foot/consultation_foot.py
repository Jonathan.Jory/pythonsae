import histoire2foot

# Ici vos fonctions dédiées aux interactions


def afficher_menu(titre, liste_options):
    """retourne un menu avec des options

    Args:
        titre (str): un titre
        liste_options (list): liste d'option

    Returns:
        str: un menu avec des options
    """
    print("+"+("-"*len(titre))+"--"+"+")
    print(f"| {titre} |")
    print("+"+("-"*len(titre))+"--"+"+")
    for i in range (len(liste_options)):
        print(f" {i+1} -> {liste_options[i]}")


def demander_nombre(message,borne_max):
    reponse_str=input(f"{message}\n")
    try:
        reponse_int=int(reponse_str)
    except:
        return None
    else:
        if reponse_int<1 or reponse_int>borne_max:
            return None
        return reponse_int


def menu(titre,liste_options,):
    afficher_menu(titre,liste_options)
    reponse=demander_nombre("Choissisez une option",len(liste_options))
    return reponse


def affichage_rep(rep):
    if rep != None:
        for elem in rep:
            print("-",elem)


#rep=2


def afficher_date(liste_matchs):
    liste_dates=[]
    for match in liste_matchs:
        if match[0] not in liste_dates:
            liste_dates.append(match[0])
    return liste_dates

def matchs_date(liste_matchs, date):
    """retourne la liste des matchs qui se sont déroulés a une date donnée
    
    Args:
        liste_matchs (list): une liste de matchs
        date (str): date 

    Returns:
        list: la liste des matchs qui se sont déroulé à la date   
    """
    liste=[]
    res=""
    for elem in liste_matchs:
        if elem[0]==date:
            liste.append(elem)
    if liste==[]:
        res=None
    else:
        res=liste
    return res


#rep3


def ville_joue(liste_matchs):
    """retourne la liste des villes
    
    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des villes  
    """
    villes=[]
    for elem in liste_matchs:
        if elem[6] not in villes:
            villes.append(elem[6])
    return villes

#rep5

def match_une_equipe(liste_matchs, nom_equipe):
    liste = []
    for elem in liste_matchs:
        if elem[1] == nom_equipe or elem[2] == nom_equipe:
            if elem not in liste:
                liste.append(elem)
    if liste == []:
        liste = None
    return liste

#rep6

def complement_prem_vict(liste_match, equipe):
    date = histoire2foot.premiere_victoire(liste_match, equipe)
    if date == None:
        print("Desoler cette équipe n'a aucune victoire")
    else:
        for match in liste_match:
            if match[0] == date and match[1] == equipe:
                adversaire = match[2]
            elif match[0] == date and match[2] == equipe:
                adversaire = match[1]
        print(f"La première victoire de l'équipe '{equipe}' à était le {date} contre l'équipe '{adversaire}'")

#rep8


def tournoi(liste_matchs):
    """retourne la liste des tournoi
    
    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des tournoi
    """
    liste_tournoi=[]
    for trn in liste_matchs:
        if trn[5] not in liste_tournoi:
            liste_tournoi.append(trn[5])
    return liste_tournoi

    
#rep9


def meilleures_equipesV(liste_matchs):
    """retourne la liste des équipes de la liste qui ont le plus grand nombre de victoires

    Args:
        liste_matchs (list): une liste de matchs

    Returns:
        list: la liste des équipes qui ont le plus grand nombre de victoires
    """
    max=0
    liste_equipes=histoire2foot.liste_des_equipes(liste_matchs)
    liste_plus_vict=[]
    for elem in liste_equipes:
        victoire=histoire2foot.resultats_equipe(liste_matchs,elem)[0]
        if max==0:
            max=victoire
        if max<victoire:
            max=victoire
    for elem in liste_equipes:
        if histoire2foot.resultats_equipe(liste_matchs,elem)[0]==max:
            liste_plus_vict.append(elem)
    return liste_plus_vict, max

    
def plus_victoire_compet(liste_matchs, nom_competition):
    """retourne la liste des équipes de la liste qui ont le plus grand nombre de victoires dans une competitions

    Args:
        liste_matchs (list): une liste de matchs
        nom_competition (str): nom de la competition

    Returns:
        list: la liste des équipes qui ont le plus grand nombre de victoires dans une competition
    """    
    equipe_in_compet = []
    for elem in liste_matchs:
        if elem[5] == nom_competition:
            equipe_in_compet.append(elem)
    if equipe_in_compet == []:
        print("Désoler aucune équipe dans cette compétition")
    else:
        meilleur = meilleures_equipesV(equipe_in_compet)
        print("Les meilleures équipes sont:")
        affichage_rep(meilleur[0])
        print(f"Avec {meilleur[1]} Victoire")

# ici votre programme principal

def programme_principal():
    liste_options = ["Charger un fichier","Trouver la liste de match à une date","Trouver la liste de match d'une ville","Lister les équipes du fichier","Afficher les matchs d'une équipe","Afficher la date de la première victoire d'une équipe","Afficher le nombre de victoire de nul et de defaite d'une équipe","Nombre moyen de but dans une compétition","Equipe avec le plus de victoire d'une compétition","Quitter\n"]
    while True:
        rep = menu("Le FOOT !",liste_options)
        if rep is None:
            print("Cette option n'existe pas")
        elif rep == 1:
            print("Vous avez choisi:",liste_options[rep-1])
            nom_fichier=input("Veuillez donner le nom du fichier à charger : ")
            try:
                open(nom_fichier)
                pass
            except (FileNotFoundError, IOError):
                print("Fichier introuvable")
                break
            liste_match=(histoire2foot.charger_matchs(nom_fichier))
            print("Dans le fichier, il y a :",len(liste_match), " matchs")
        elif rep == 2:
            try:
                type(liste_match) == list
            except:
                print("Vous n'avez pas chargé un fichier")
                input("Appuyez sur entrée")
                continue
            print("Vous avez choisi:",liste_options[rep-1])
            print("Les dates disponibles sont:\n")
            affichage_rep(afficher_date(liste_match))
            res=matchs_date(liste_match, input("Donnez une date comme ce-ci: 1978-06-18 \n A vous: "))
            if res==None:
                print("Aucun match à cette date")
            else:
                print("Les matchs sont:")
                affichage_rep(res)
        elif rep == 3:
            try:
                type(liste_match) == list
            except:
                print("Vous n'avez pas chargé un fichier")
                input("Appuyez sur entrée")
                continue
            print("Vous avez choisi:",liste_options[rep-1])
            print("Les villes disponibles sont:\n") 
            affichage_rep(ville_joue(liste_match))
            res = histoire2foot.matchs_ville(liste_match,input("Quel est la ville ?\n"))
            if res == []:
                print("Aucun match dans cette ville")
            else:
                print(f"Les matchs sont:")
                affichage_rep(res)
        elif rep == 4:
            try:
                type(liste_match)==list
            except:
                print("Vous n'avez pas chargé un fichier")
                input("Appuyez sur entrée")
                continue
            print("Vous avez choisi:",liste_options[rep-1])
            print("Les équipes sont:")
            affichage_rep(histoire2foot.liste_des_equipes(liste_match))
        elif rep == 5:
            try:
                type(liste_match) == list
            except:
                print("Vous n'avez pas chargé un fichier")
                input("Appuyez sur entrée")
                continue
            print("Vous avez choisi:",liste_options[rep-1])
            res = match_une_equipe(liste_match, input("Quel est l'équipe que vous voulez ?\n"))
            if res == None:
                print("Cette équipe n'a joué aucun match")
            else:
                print(f"Il y a {len(res)} match joué, voici la listes:")
                affichage_rep(res)
        elif rep == 6:
            try:
                type(liste_match) == list
            except:
                print("Vous n'avez pas chargé un fichier")
                input("Appuyez sur entrée")
                continue
            print("Vous avez choisi:",liste_options[rep-1])
            complement_prem_vict(liste_match, input("Quel est l'équipe ? \n"))
        elif rep == 7:
            try:
                type(liste_match) == list
            except:
                print("Vous n'avez pas chargé un fichier")
                input("Appuyez sur entrée")
                continue
            print("Vous avez choisi:",liste_options[rep-1])
            res = histoire2foot.resultats_equipe(liste_match,input("Quel est l'équipe ?\n"))
            if res == None:
                print("Aucune équipe de ce nom")
            else:
                print(f"Les résultats de cette équipe sont: {res[0]} victoires, {res[1]} match nul et {res[2]} défaites")
        elif rep == 8:
            try:
                type(liste_match) == list
            except:
                print("Vous n'avez pas chargé un fichier")
                input("Appuyez sur entrée")
                continue
            print("Vous avez choisi:",liste_options[rep-1])
            print("Les différentes tournois sont:")
            affichage_rep(tournoi(liste_match))
            res=histoire2foot.nombre_moyen_buts(liste_match, input("Quel est le tournoi que vous souhaitez consulter ?\n"))
            if res == None:
                print("Aucun tournoi de ce nom")
            else:
                print(f"Le nombre moyen de but dans ce tournoi est de : {res} par match.")
        elif rep == 9:
            try:
                type(liste_match) == list
            except:
                print("Vous n'avez pas chargé un fichier")
                input("Appuyez sur entrée")
                continue
            print("Vous avez choisi:",liste_options[rep-1])
            print("Les différentes tournois sont:")
            affichage_rep(tournoi(liste_match))
            plus_victoire_compet(liste_match,input("Quel est le nom de la competition ?\n"))
        elif rep == 10:
            break
        input("\nAppuyer sur Entrée pour continuer\n")
    print("Merci au revoir!")


programme_principal()